function flattenArray(elements,depth){
   if(depth=== undefined){
    depth=1;
   }
    let newFlattenArray=[];
    for(let index=0; index<elements.length; index++){

        if((Array.isArray(elements[index])) && depth>0){
            let flatArr=flattenArray(elements[index], depth-1);
            newFlattenArray=newFlattenArray.concat(flatArr);

        }else{
            if(elements[index]!== undefined){
            newFlattenArray.push(elements[index]);
            }
        }
    }
    return newFlattenArray;
}
module.exports=flattenArray;