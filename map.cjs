function map(elements, cb){
     let newItemArray=[];
      if(Array.isArray(elements)=== false){
        return;
      }  
     for(let index=0; index< elements.length; index++){
        newItemArray.push(cb(elements[index],index,elements));
        
     }
     return newItemArray;
}
module.exports=map;