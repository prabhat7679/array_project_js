function each(elements, cb){
    if(Array.isArray(elements)===false){
        return false;
    }

    for(let index=0; index<elements.length; index++){
        cb(elements[index], index);
    }
}
module.exports=each;