function find(elements, cb){
    if(Array.isArray(elements)=== false){
        return;
    }
    // let count=0;
    for(let index=0;index< elements.length; index++){
        if(cb(elements[index])=== true){
           // count++;
            return elements[index];
        }
    }
    return;
}
module.exports=find;