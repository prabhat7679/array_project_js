function filter(elements, cb){
    let newFilterArray=[];
    if(Array.isArray(elements)=== false){
        return;
    }
    
    for(let index=0;index< elements.length; index++){
        if(cb(elements[index],index, elements)=== true){
           
            newFilterArray.push(elements[index]);
        }
    }
    
    return newFilterArray;
}
module.exports=filter;