function reduce(elements, cb, startingValue){
    let myIndex=0;
    if(startingValue===undefined){
        startingValue=elements[0];
        myIndex=1;
    }

    for(let index=myIndex; index< elements.length; index++){
        if(elements[index]!==undefined){
        startingValue = cb(startingValue, elements[index],index, elements);

        }
    }
    return startingValue;
}
module.exports=reduce;